# Tic-tac-toe

## Веб сайт для игры в крестики нолики
Проект был выполнен в ходе прохождения школы программирования от компании ESoft.

## Стек технллогий
- React
- MOBX
- Express
- PostgreSQL
- WebSockets
- Docker
