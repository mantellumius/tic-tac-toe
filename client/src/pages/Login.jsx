import React from "react";
import LoginForm from "../components/LoginForm/LoginForm";
import MainContainer from "../components/MainContainer/MainContainer";

const Login = () => {
	return (
		<MainContainer>
			<LoginForm/>
		</MainContainer>
	);
};

export default Login;