import React, {useEffect, useState} from "react";
import Table from "../../components/Table/Table";
import MainContainer from "../../components/MainContainer/MainContainer";
import classes from "./History.module.css";
import {Header, Icon, Paper} from "../../UI";
import $api from "../../api";
import {FormatDate, FormatDiff} from "../../helpers/DateFormatter";

const History = () => {
	const [games, setGames] = useState([]);
	const [reformattedGames, setReformattedGames] = useState([]);
	useEffect(() => {
		$api.get("protected/history").then(res => {
			setGames(
				res.data.map(game => {
					return {
						name1: `${game.surNameO} ${game.firstNameO.charAt(0)}. ${game.secondNameO.charAt(0)}.`,
						name2: `${game.surNameX} ${game.firstNameX.charAt(0)}. ${game.secondNameX.charAt(0)}.`,
						winner: game.gameResult ? 2 : 1,
						date: `${FormatDate(game.gameEnd)}`,
						duration: `${FormatDiff(new Date(game.gameEnd) - new Date(game.gameBegin))}`,
					};
				}));
		})
			.catch(() => {});

	}, []);
	useEffect(() => {
		setReformattedGames(games.map(game => {
				return {
					players: <div className={classes.playersCell}>
						<div className={`${classes.person} ${classes.first}`}>
							<Icon icon="images/zero.svg" width={16} height={16}/>
							{`${game.name1}    `}
							{game.winner === 1 && <Icon icon="images/cup.svg"/>}
						</div>
						<span className={classes.vs}>против</span>
						<div className={classes.person}>
							<Icon icon="images/x.svg" width={16} height={16}/>
							{game.name2}
							{game.winner === 2 && <Icon icon="images/cup.svg"/>}
						</div>
					</div>,
					date: game.date,
					duration: game.duration
				};
			}
		));
	}, [games]);
	let headers = [
		"Игроки",
		"Дата",
		"Время игры",
	];

	return (
		<MainContainer>
			<Paper className={classes.history} width={1074}>
				<Header>История игр</Header>
				<Table headers={headers} rows={reformattedGames} keysOrder={["players", "date", "duration"]}/>
			</Paper>
		</MainContainer>
	);
};

export default History;