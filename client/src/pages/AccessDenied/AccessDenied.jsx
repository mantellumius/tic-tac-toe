import React from "react";
import {Header} from "../../UI";
import MainContainer from "../../components/MainContainer/MainContainer";

const AccessDenied = () => {
	return (
		<MainContainer>
			<Header>У вас нет прав для доступа к данной странице</Header>
		</MainContainer>
	);
};

export default AccessDenied;