import React, {useEffect, useState} from "react";
import classes from "./ActivePlayers.module.css";
import {ChipsS, Header, Paper, PrimaryButton, Switch} from "../../UI";
import MainContainer from "../../components/MainContainer/MainContainer";
import List from "../../components/List/List";
import {useAuthStore} from "../../stores/AuthStore";
import $api from "../../api";

const ActivePlayers = ({playgroundStore}) => {
	const authStore = useAuthStore();
	const [players, setPlayers] = useState([]);
	const [reformattedPlayers, setReformattedPlayers] = useState([]);
	useEffect(() => {
		$api.get("/protected/active-players")
			.then(res => {
				setPlayers(res.data.filter(player => player.id !== authStore.userId));
			})
			.catch(() => {
			});
	}, []);
	useEffect(() => {
		reformatPlayers(players);
	}, [players]);

	function invite(playerId) {
		playgroundStore.invite(playerId);
	}

	function reformatPlayers(players) {
		setReformattedPlayers(players.map(player => {
			return {
				name: `${player.surName} ${player.firstName} ${player.secondName}`,
				statusIsInGame: <div className={classes.status}>
					<ChipsS type={player.statusIsInGame ? "playing" : "ready"}/>
					<PrimaryButton isDisabled={player.statusIsInGame}
					               text={"Позвать играть"}
					               onClick={() => invite(player.id)}/>
				</div>
			};
		}));
	}

	function onlyFreePlayers(on) {
		if (on) reformatPlayers(players.filter(player => !player.statusIsInGame)); else reformatPlayers(players);
	}

	return (<MainContainer>
		<Paper className={classes.activePlayers} width={781}>
			<div className={classes.head}>
				<Header>Активные игроки</Header>
				<Switch text="Только свободные" callback={onlyFreePlayers}/>
			</div>
			<List rows={reformattedPlayers} keysOrder={["name", "statusIsInGame"]}/>
		</Paper>
	</MainContainer>);
};

export default ActivePlayers;