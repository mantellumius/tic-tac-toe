import React from "react";
import classes from "./Playground.module.css";
import {StoreProvider} from "../../stores/PlaygroundStore";
import Game from "../../components/Playground/Game/Game";
import Chat from "../../components/Chat/Chat";
import Players from "../../components/Players/Players";
import {observer} from "mobx-react-lite";

const Playground = observer(({playgroundStore}) => {
	return (
		<StoreProvider store={playgroundStore}>
			<div className={classes.container}>
				<div className={classes.playground}>
					<Players players={playgroundStore.playersInfo}/>
					<Game/>
					<Chat/>
				</div>
			</div>
		</StoreProvider>
	);
});

export default Playground;