import React, {useEffect, useMemo, useState} from "react";
import classes from "./PlayersList.module.css";
import {ChipsM, Header, Icon, Paper, PrimaryButton, SecondaryButton} from "../../UI";
import MainContainer from "../../components/MainContainer/MainContainer";
import AddPlayerModal from "../../components/AddPlayerModal/AddPlayerModal";
import Table from "../../components/Table/Table";
import {FormatDate} from "../../helpers/DateFormatter";
import $api from "../../api";

const PlayersList = () => {
	const [players, setPlayers] = useState([]);
	const [isAddPlayerModalOpen, setIsAddPlayerModalOpen] = useState(false);
	useEffect(() => {
		$api.get("/protected/users")
			.then(res => setPlayers(res.data))
			.catch(() => {});
	}, []);
	const headers = ["ФИО", "Возраст", "Пол", "Статус", "Создан", "Изменён", ""];
	const reformattedPlayers = useMemo(() => players.map(player => {
		return {
			name: `${player.surName} ${player.firstName} ${player.secondName}`,
			age: player.age,
			gender: <Icon icon={`images/${player.gender ? "man" : "woman"}.svg`}/>,
			statusIsBlocked: player.statusIsBlocked ? <ChipsM type="blocked"/> : <ChipsM type="ready"/>,
			button:
				<SecondaryButton onClick={() => switchBlocked(player.id)} width={180}>
					{player.statusIsBlocked ? "Разблокировать" : <><Icon icon="images/block.svg"/> Заблокировать</>}
				</SecondaryButton>,
			createdAt: FormatDate(player.createdAt),
			updatedAt: FormatDate(player.updatedAt),
		};
	}), [players]);

	async function switchBlocked(id) {
		let player = players.find(player => player.id === id);
		await $api.patch(`/protected/users/${id}`, {statusIsBlocked: !player.statusIsBlocked});
		setPlayers(players.map(player => player.id === id ? {
			...player,
			statusIsBlocked: !player.statusIsBlocked
		} : player));
	}

	async function onAddPlayerSubmit(newPlayer) {
		let fio = newPlayer.name.split(" ");
		newPlayer.surName = fio[0];
		newPlayer.firstName = fio[1];
		newPlayer.secondName = fio[2];
		newPlayer.gender = newPlayer.gender === "man";
		delete newPlayer.name;
		let result = await $api.post("/protected/users", newPlayer);
		if (result.status === 200)
			setPlayers([...players, newPlayer]);
	}

	return (
		<MainContainer>
			{isAddPlayerModalOpen &&
				<AddPlayerModal setIsOpen={setIsAddPlayerModalOpen} onSubmit={onAddPlayerSubmit}/>}
			<Paper className={classes.playersList} width={1416}>
				<div className={classes.head}>
					<Header>Список игроков</Header>
					<PrimaryButton text="Добавить игрока" onClick={() => setIsAddPlayerModalOpen(true)}/>
				</div>
				<Table headers={headers} rows={reformattedPlayers}
				       keysOrder={["name", "age", "gender", "statusIsBlocked", "createdAt", "updatedAt", "button"]}/>
			</Paper>
		</MainContainer>);
};

export default PlayersList;