import React, {useEffect, useState} from "react";
import classes from "./Rating.module.css";
import {Header, Paper} from "../../UI";
import Table from "../../components/Table/Table";
import MainContainer from "../../components/MainContainer/MainContainer";
import $api from "../../api";

const Rating = () => {
	const [players, setPlayers] = useState([]);
	useEffect(() => {
		$api.get("/protected/ratings")
			.then(res => {
				let data = res?.data;
				data = data?.map((row) => {
					return {
						name: `${row.surName} ${row.firstName} ${row.secondName}`,
						total: row.total,
						wins: row.wins,
						loses: row.loses,
						win_rate: (row.win_rate * 100).toFixed(2) + "%"
					};
				});
				if (data) setPlayers(data);
			});
	}, []);
	let headers = [
		"ФИО",
		"Всего игр",
		"Победы",
		"Проигрыши",
		"Процент побед",
	];

	return (
		<MainContainer>
			<Paper className={classes.rating} width={1026}>
				<Header>Рейтинг игроков</Header>
				<Table headers={headers} rows={players} keysOrder={["name", "total", "wins", "loses", "win_rate"]}/>
			</Paper>
		</MainContainer>
	);
};

export default Rating;