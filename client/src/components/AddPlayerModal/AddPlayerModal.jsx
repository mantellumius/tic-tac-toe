import React, {useState} from "react";
import {Header, Icon, IconButton, Input, InputSet, Label, Modal, PrimaryButton, RadioButtonSet} from "../../UI";
import Row from "../Row/Row";

const AddPlayerModal = ({setIsOpen, onSubmit}) => {
	const [newPlayer, setNewPlayer] = useState(() => getDefaultPlayer());
	const [gender, setGender] = useState([
		{value: "woman", boolean: true, content: <Icon icon="images/woman.svg"/>},
		{value: "man", boolean: false, content: <Icon icon="images/man.svg"/>}
	]);

	function getDefaultPlayer() {
		return {name: "", age: "", gender: "woman"};
	}

	function onlyNumericInput(event) {
		if (RegExp(/^\d*\d*$/).test(event.target.value))
			event.target.oldValue = event.target.value;
		else
			event.target.value = event.target.oldValue ?? "";
	}

	function addNewPlayer() {
		if (!isValid(newPlayer)) return;
		onSubmit(newPlayer);
		setNewPlayer(getDefaultPlayer());
	}

	function isValid(newPlayer) {
		return newPlayer.age && newPlayer.name && newPlayer.name.split(" ").length === 3;
	}

	return (
		<Modal width={400} height={400} setIsOpen={setIsOpen}>
			<Row justifyContent="end">
				<IconButton icon="images/close.svg" onClick={() => setIsOpen(false)}/>
			</Row>
			<Header>Добавить игрока</Header>
			<InputSet>
				<Label text="ФИО"/>
				<Input onChange={(event) => setNewPlayer({...newPlayer, name: event.target.value})}
				       value={newPlayer.name} placeholder="Иванов Иван Иванович"></Input>
			</InputSet>
			<Row>
				<Row gap={40}>
					<InputSet width={72}>
						<Label text="Возраст"/>
						<Input onChange={(event) => {
							onlyNumericInput(event);
							setNewPlayer({...newPlayer, age: event.target.value});
						}}
						       value={newPlayer.age} placeholder={0} width={72}/>
					</InputSet>
					<InputSet>
						<Label text="Пол"/>
						<RadioButtonSet onChange={(value) => setNewPlayer({...newPlayer, gender: value})}
						                settings={gender}
						                setSettings={setGender}/>
					</InputSet>
				</Row>
			</Row>
			<PrimaryButton width="100%" onClick={() => {
				setIsOpen(false);
				addNewPlayer();
			}} isDisabled={!newPlayer.age || !newPlayer.name}>Добавить</PrimaryButton>
		</Modal>
	);
};

export default AddPlayerModal;