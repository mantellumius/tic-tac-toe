import React from "react";
import {useAuthStore} from "../stores/AuthStore";
import {Navigate} from "react-router-dom";

const ProtectedRoute = ({element, requiredRole}) => {
	const authStore = useAuthStore();
	if (!authStore.isAuth)
		return <Navigate to="/login" replace/>;
	if (!authStore.haveAccess(requiredRole))
		return <Navigate to="/access_denied" replace/>;
	return element;
};

export default ProtectedRoute;