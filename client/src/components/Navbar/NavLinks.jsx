import React from "react";
import {Link} from "react-router-dom";
import classnames from "./Navbar.module.css";

const NavLinks = ({navLinks}) => {
	return (
		<>
			{navLinks.map((navLink) =>
				<Link key={navLink.href}
				      className={`${classnames.link} ${navLink.href === location.pathname && classnames.active}`}
				      to={navLink.href}>{navLink.text}
				</Link>)}
		</>
	);
};

export default NavLinks;
