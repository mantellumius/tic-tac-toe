import React from "react";
import {useLocation} from "react-router-dom";
import classnames from "./Navbar.module.css";
import {Icon, IconButton} from "../../UI";
import {useAuthStore} from "../../stores/AuthStore";
import NavLinks from "./NavLinks";

const Navbar = () => {
	const authStore = useAuthStore();
	const navLinks = [
		{
			href: "/playground",
			text: "Игровое поле",
		},
		{
			href: "/rating",
			text: "Рейтинг",
		},
		{
			href: "/active_players",
			text: "Активные игроки",
		},
		{
			href: "/history",
			text: "История игр",
		},
		{
			href: "/players_list",
			text: "Список игроков",
		},
	];

	const location = useLocation();

	function logout() {
		authStore.logout();
		window.location.reload();
	}

	return location.pathname !== "/login" &&
		<header className="elevation-1">
			<Icon icon="images/s-logo.svg"/>
			<div className={classnames.navPanel}>
				<NavLinks navLinks={navLinks}/>
			</div>
			<IconButton icon="images/sign-out-icon.svg" onClick={logout}/>
		</header>;
};

export default Navbar;