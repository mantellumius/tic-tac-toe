import React from "react";
import classes from "./List.module.css";

const List = ({rows, keysOrder}) => {
	return (
		<div className={classes.list}>
			{rows && rows.map((row, index) =>
				<div key={index} className={classes.row}>
					{keysOrder.map((key, i) => <div key={`${index} ${i}`}>{row[key]}</div>)}
				</div>)
			}
		</div>
	);
};

export default List;