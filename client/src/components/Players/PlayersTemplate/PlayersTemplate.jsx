import React from "react";
import classes from "../Players.module.css";
import RectangleTemplate from "../../../templates/RectangleTemplate/RectangleTemplate";

const PlayersTemplate = () => {
	return (
		<div className={classes.container}>
			<RectangleTemplate width={358} height={48}/>
			<RectangleTemplate width={358} height={48}/>
		</div>
	);
};

export default PlayersTemplate;