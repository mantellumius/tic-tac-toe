import React from "react";
import classes from "./Players.module.css";
import PlayerContainer from "./PlayerContainer/PlayerContainer";
import {Header, Paper} from "../../UI";
import PlayersTemplate from "./PlayersTemplate/PlayersTemplate";

const Players = ({players}) => {
	return (
		<Paper className={classes.players}>
			<Header>Игроки</Header>
			{players === null ?
				<PlayersTemplate/> :
				<div className={classes.container}>
					<PlayerContainer icon={"images/x.svg"}
					                 name={players.get("x").name} winRate={players.get("x").winRate}/>
					<PlayerContainer icon={"images/zero.svg"}
					                 name={players.get("o").name} winRate={players.get("o").winRate}/>
				</div>}
		</Paper>
	);
};

export default Players;