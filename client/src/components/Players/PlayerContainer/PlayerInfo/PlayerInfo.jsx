import React from "react";
import classes from "./PlayerInfo.module.css";

const PlayerInfo = ({name, winRate}) => {
	return (
		<div className={classes.playerInfo}>
			<div className={classes.name}>{name}</div>
			<div className={classes.winRate}>{winRate}% побед</div>
		</div>
	);
};

export default PlayerInfo;