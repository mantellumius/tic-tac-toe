import React from "react";
import PlayerInfo from "./PlayerInfo/PlayerInfo";
import classes from "./PlayerContainer.module.css";
import {Icon} from "../../../UI";

const PlayerContainer = ({icon, name, winRate}) => {
	return (
		<div className={classes.playerContainer}>
			<Icon icon={icon} width={24} height={24}/>
			<PlayerInfo name={name} winRate={winRate}/>
		</div>
	);
};

export default PlayerContainer;