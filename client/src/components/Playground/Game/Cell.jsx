import React from "react";
import classes from "./GameField.module.css";
import {Icon} from "../../../UI";
import {observer} from "mobx-react-lite";

const Cell = observer(({x, y, onClick, sign, props, isPartOfWinCombination}) => {
	const xImg = "../images/xxl-x.svg";
	const oImg = "../images/xxl-zero.svg";
	let src = sign && (sign === "x" ? xImg : oImg);

	return (
		<div {...props} className={`${classes.cell} ${isPartOfWinCombination && (sign === "x" ? classes.cellXWin : classes.cellOWin)}`} onClick={() => onClick(x, y)}>
			<Icon icon={src} alt=""/>
		</div>
	);
});

export default Cell;