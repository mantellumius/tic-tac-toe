import React from "react";
import classes from "./GameField.module.css";
import {Icon, Paper} from "../../../UI";
import {observer} from "mobx-react-lite";

const ActivePlayer = observer(({name, mode}) => {
	const xSmallImg = "../images/x.svg";
	const oSmallImg = "../images/zero.svg";
	return (
		<Paper className={classes.gameStep}>
			{name && mode && <>
				Ходит <Icon icon={mode === "x" ? xSmallImg : oSmallImg}
				            width={27} height={27}
				            alt=""/> {name}</>}

		</Paper>
	);
});

export default ActivePlayer;