import React, {useEffect, useState} from "react";
import classes from "./GameField.module.css";
import {observer} from "mobx-react-lite";

const Stopwatch = ({isRunning, startFrom}) => {
	const [time, setTime] = useState(0);

	useEffect(() => {
		let intervalId;
		if (isRunning) {
			intervalId = setInterval(() => setTime(time + 1), 1000);
		}
		return () => clearInterval(intervalId);
	}, [isRunning, time]);
	useEffect(() => setTime(startFrom || 0), [startFrom]);
	// const hours = Math.floor(time / 3600);
	const minutes = Math.floor((time / 60));
	const seconds = Math.floor((time % 60));


	return (
		<div className={classes.gameTime}>
			{`${minutes.toString().padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`}
		</div>
	);
};

export default observer(Stopwatch);