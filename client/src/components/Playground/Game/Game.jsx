import React, {useEffect, useState} from "react";
import ActivePlayer from "./ActivePlayer";
import Stopwatch from "./Stopwatch";
import GameBoard from "./GameBoard";
import classes from "./GameField.module.css";
import {observer} from "mobx-react-lite";
import {usePlaygroundStore} from "../../../stores/PlaygroundStore";
import {useAuthStore} from "../../../stores/AuthStore";
import WinnerModal from "./WinnerModal/WinnerModal";
import {useNavigate} from "react-router-dom";

const Game = () => {
	const playgroundStore = usePlaygroundStore();
	const authStore = useAuthStore();
	const navigate = useNavigate();
	const [isModalOpened, setIsModalOpened] = useState(false);
	useEffect(() => {
		setIsModalOpened(playgroundStore.isGameOver);
	}, [playgroundStore.isGameOver]);

	function newGame() {
		const players = Array.from(playgroundStore.playersInfo.values());
		const opponent = players.find(player => player.id !== authStore.userId);
		playgroundStore.invite(opponent.id);
	}

	function exit() {
		navigate("/active_players");
	}

	return (
		<div className={classes.gameContainer}>
			{isModalOpened && playgroundStore.matchResult &&
				<WinnerModal newGame={newGame} exit={exit} nickname={playgroundStore.matchResult.winnerNickname} setIsOpen={setIsModalOpened}/>}
			<Stopwatch isRunning={!playgroundStore.isGameOver && playgroundStore.playersInfo} startFrom={(new Date(Date.now()) - new Date(playgroundStore.gameBegin)) / 1000}/>
			<GameBoard isGameOver={playgroundStore.isGameOver}
			           winCombination={playgroundStore.matchResult?.winnersCombination}
			           state={playgroundStore.state}
			           makeMove={playgroundStore.makeMove.bind(playgroundStore)}
			           userId={authStore.userId}/>
			<ActivePlayer
				name={playgroundStore.playersInfo?.get(playgroundStore.mode).nickname}
				mode={playgroundStore.mode}/>
		</div>
	);
};

export default observer(Game);