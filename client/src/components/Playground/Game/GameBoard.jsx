import React from "react";
import Cell from "./Cell";
import classes from "./GameField.module.css";

const GameBoard = ({state, winCombination, makeMove, userId}) => {
	return (
		<div className={classes.gameBoard}>
			{state.map((row, y) =>
				row.map((cell, x) =>
					<Cell key={`${x} ${y}`} x={x} y={y} sign={state[y][x]}
					      isPartOfWinCombination={winCombination?.includes(y * 3 + x)}
					      onClick={(x, y) => makeMove(userId, x, y)}/>))}
		</div>
	);
};

export default GameBoard;