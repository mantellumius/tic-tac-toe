import React from "react";
import classes from "./WinnerModal.module.css";
import {Header, Icon, Modal, PrimaryButton, SecondaryButton} from "../../../../UI";

const WinnerModal = ({newGame, exit, nickname, setIsOpen}) => {
	return (
		<Modal width={400} height={430} className={classes.winnerModal} setIsOpen={setIsOpen}>
			<Icon icon={"images/trophy.png"} width={132} height={165}/>
			<Header className={classes.header}>{nickname ? `${nickname} победил(а)!` : "Ничья"}</Header>
			<div className={classes.buttons}>
				<PrimaryButton text="Новая игра" width="100%" style={{marginBottom: "12px"}} onClick={newGame}/>
				<SecondaryButton text="Выйти в меню" width="100%" height="48px" onClick={exit}/>
			</div>
		</Modal>
	);
};

export default WinnerModal;