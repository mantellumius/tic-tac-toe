import React from "react";
import {Navigate, Route, Routes} from "react-router-dom";
import ProtectedRoute from "../ProtectedRoute";
import Rating from "../../pages/Rating/Rating";
import Playground from "../../pages/Playground/Playground";
import ActivePlayers from "../../pages/ActivePlayers/ActivePlayers";
import History from "../../pages/History/History";
import PlayersList from "../../pages/PlayersList/PlayersList";
import Login from "../../pages/Login";
import AccessDenied from "../../pages/AccessDenied/AccessDenied";

const Router = ({stores}) => {
	return (
		<Routes>
			<Route path={"/rating"} element={
				<ProtectedRoute element={<Rating/>}/>}/>
			<Route path={"/playground"} element={
				<ProtectedRoute element={<Playground playgroundStore={stores.playgroundStore}/>}/>}/>
			<Route path={"/active_players"} element={
				<ProtectedRoute element={<ActivePlayers playgroundStore={stores.playgroundStore}/>}/>}/>
			<Route path={"/history"} element={
				<ProtectedRoute element={<History/>}/>}/>
			<Route path={"/players_list"} element={
				<ProtectedRoute element={<PlayersList/>} requiredRole={"admin"}/>}/>
			<Route path={"/login"} element={<Login/>}/>
			<Route path={"/access_denied"} element={<AccessDenied/>}/>
			{!stores.authStore.isAuth && <Route path={"*"} element={<Navigate to={"/login"} replace/>}/>}
		</Routes>
	);
};

export default Router;