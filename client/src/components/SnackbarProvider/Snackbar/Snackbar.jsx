import React from "react";
import classes from "./Snackbar.module.css";
import {IconButton} from "../../../UI";
import Row from "../../Row/Row";

const Snackbar = ({text, children, remove}) => {
	return (
		<div className={classes.snackbar}>
			<Row gap={20} alignItems={"center"}>
				{text}
				{children}
				<IconButton icon={"/images/close.svg"} color={"white"} onClick={() => remove()}/>
			</Row>
		</div>
	);
};

export default Snackbar;