import React from "react";
import classes from "./SnackbarProvider.module.css";
import Snackbar from "./Snackbar/Snackbar";
import {PrimaryButton} from "../../UI";
import {observer} from "mobx-react-lite";
import {StoreProvider} from "../../stores/SnackbarStore";

const SnackbarProvider = ({snackbarStore}) => {
	return (
		<StoreProvider store={snackbarStore}>
			<div className={classes.snackbarProvider}>
				{Array.from(snackbarStore.snackbars.values()).map(snackbar =>
					<Snackbar key={snackbar.id} text={snackbar.text} remove={snackbar.remove}>
						{snackbar.action &&
							<PrimaryButton text={snackbar.action.text} onClick={snackbar.action.callback}
							               width={120} height={48}/>}
					</Snackbar>)}
			</div>
		</StoreProvider>
	);
};

export default observer(SnackbarProvider);