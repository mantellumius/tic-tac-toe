import React from "react";
import InteractiveElements from "./InteractiveElements/InteractiveElements";
import classes from "./Chat.module.css";
import {usePlaygroundStore} from "../../stores/PlaygroundStore";
import {useAuthStore} from "../../stores/AuthStore";
import MessagesContainer from "./MessagesContainer/MessagesContainer";

const Chat = () => {
	const playgroundStore = usePlaygroundStore();
	const authStore = useAuthStore();

	function sendMessage(text) {
		playgroundStore.sendMessage(authStore.userId, text);
	}

	return (
		<div className={classes.chatContainer}>
			<MessagesContainer messages={playgroundStore.messages} userId={authStore.userId}/>
			<InteractiveElements send={(text) => sendMessage(text)}/>
		</div>
	);
};

export default Chat;