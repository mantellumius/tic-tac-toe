import React, {useEffect, useRef, useState} from "react";
import classes from "./MessagesContainerNew.module.css";
import {observer} from "mobx-react-lite";
import Message from "../Message/Message";

const MessagesContainerNew = ({messages}) => {
	const container = useRef();
	const [isTopOverflowing, setIsTopOverflowing] = useState(false);
	const [isBottomOverflowing, setIsBottomOverflowing] = useState(false);
	useEffect(() => {
		if (container.current && messages[messages.length - 1]?.side === "me")
			container.current.scrollTop = Number.MAX_SAFE_INTEGER;
	}, [messages.length]);

	function onScroll() {
		setIsTopOverflowing(container.current?.scrollTop * -1 < container.current?.scrollHeight - container.current?.clientHeight);
		setIsBottomOverflowing(container.current?.scrollTop !== 0);
	}

	return (
		<>{
			messages?.length ?
				<div className={classes.container}>
					<div className={`${classes.fadeTop} ${isTopOverflowing ? classes.visible : classes.hidden}`}>
					</div>
					<div className={`${classes.fadeBottom} ${isBottomOverflowing ? classes.visible : classes.hidden}`}>
					</div>
					<div
						className={`${classes.msgsContainer}`}
						onScroll={() => onScroll()}
						ref={container}>
						{messages.slice().reverse().map(message => <Message key={message.id} side={message.side}
						                                                    time={message.time}
						                                                    mode={message.mode} author={message.author}
						                                                    text={message.text}/>)}
					</div>
				</div> :
				<div className={classes.noMessages}>Сообщеники нет</div>
		}</>);
};

export default observer(MessagesContainerNew);