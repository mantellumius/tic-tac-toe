import React from "react";
import classes from "./Message.module.css";

const Message = ({mode, author, time, text, side}) => {
	return (
		<div className={`${classes.msgContainer} elevation-1 ${side === "me" ? classes.me : classes.other}`}>
			<div className={classes.msgHeader}>
				<div className={`${classes.name} ${mode === "x" ? classes.x : classes.o}`}>{author}</div>
				<div className={classes.time}>{time}</div>
			</div>
			<div className={classes.msgBody}>{text}</div>
		</div>
	);
};

export default Message;