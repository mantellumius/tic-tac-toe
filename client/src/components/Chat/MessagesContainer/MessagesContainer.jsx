import React, {useEffect, useRef, useState} from "react";
import classes from "./MessagesContainer.module.css";
import {observer} from "mobx-react-lite";
import Message from "../Message/Message";

const MessagesContainer = ({messages}) => {
	const container = useRef();
	const [isTopOverflowing, setIsTopOverflowing] = useState(false);
	const [isBottomOverflowing, setIsBottomOverflowing] = useState(false);
	useEffect(() => {
		if (container.current && messages[messages.length - 1]?.side === "me")
			container.current.scrollTop = Number.MAX_SAFE_INTEGER;
	}, [messages.length]);

	function onScroll() {
		setIsTopOverflowing(container.current?.scrollTop !== 0);
		setIsBottomOverflowing(container.current?.scrollTop < container.current?.scrollHeight - container.current?.clientHeight);
	}

	return (
		<>{
			messages?.length ?
				<div
					className={`${classes.msgsContainer} ${isTopOverflowing && classes.isTopOverflowing} ${isBottomOverflowing && classes.isBottomOverflowing}`}
					onScroll={() => onScroll()}
					ref={container}>
					{messages.map(message => <Message key={message.id} side={message.side} time={message.time}
					                                  mode={message.mode} author={message.author}
					                                  text={message.text}/>)}
				</div> :
				<div className={classes.noMessages}>Сообщеники нет</div>
		}</>);
};

export default observer(MessagesContainer);