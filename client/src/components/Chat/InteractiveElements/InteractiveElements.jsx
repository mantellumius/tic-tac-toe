import React, {useState} from "react";
import classes from "./InteractiveElements.module.css";
import {PrimaryButton, TextArea} from "../../../UI";

const InteractiveElements = ({send}) => {
	const [text, setText] = useState("");

	function submit() {
		if (!text) return;
		send(text);
		setText("");
	}

	return (
		<div className={classes.msgInteractiveElements}>
			<TextArea placeholder="Сообщение..." value={text}
			          onChange={(e) => setText(e.target.value)}
			          onKeyDown={(e) => {
				          if (e.key === "Enter" && !e.shiftKey) {
					          e.preventDefault();
					          submit();
				          }
			          }}/>
			<PrimaryButton className={classes.button}
			               onClick={() => submit()}>
				<img src="images/send-btn.svg" alt=""/>
			</PrimaryButton>
		</div>
	);
};

export default InteractiveElements;