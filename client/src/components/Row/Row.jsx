import React from "react";
import classes from "./Row.module.css";

const Row = ({children, gap, justifyContent, alignItems}) => {
	return (
		<div className={classes.row} style={{gap: gap, justifyContent: justifyContent,  alignItems: alignItems}}>
			{children}
		</div>
	);
};

export default Row;