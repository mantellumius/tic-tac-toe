import React, {useEffect, useState} from "react";
import classes from "./LoginForm.module.css";
import {useNavigate} from "react-router-dom";
import {Header, Input, Paper, PrimaryButton} from "../../UI";
import {useAuthStore} from "../../stores/AuthStore";

const LoginForm = () => {
	const authStore = useAuthStore();
	const navigate = useNavigate();
	const [invalidInput, setInvalidInput] = useState(false);
	const [submitButtonDisabled, setSubmitButtonDisabled] = useState(false);
	const [user, setUser] = useState({login: "", password: ""});
	useEffect(() => setSubmitButtonDisabled(!user.login || !user.password), [user]);

	function onChange(event) {
		(invalidInput && setInvalidInput(false));
		if (RegExp(/^[a-zA-Z\d._]*$/).test(event.target.value))
			event.target.oldValue = event.target.value;
		else
			event.target.value = event.target.oldValue ?? "";
		setUser({...user, [event.target.name]: event.target.value});
	}

	async function onSubmit(event) {
		event.preventDefault();
		let authResult = await authStore.login(user);
		if (authResult)
			navigate("/rating");
		else
			setInvalidInput(true);
	}

	return (
		<Paper className={classes.login} width={400}>
			<img className={classes.dogImage} src="images/dog.png" alt=""/>
			<Header>Войдите в игру</Header>
			<form action="/client/src/pages/Login" method="POST" className={classes.loginForm} onSubmit={onSubmit}>
				<Input id="login" type="text" placeholder="Логин" name="login" isInputInvalid={invalidInput}
				       value={user.login} onChange={(event) => onChange(event)}
				       textInputInvalid="Неверный логин"/>
				<Input id="password" type="password" placeholder="Пароль" name="password" isInputInvalid={invalidInput}
				       value={user.password} onChange={(event) => onChange(event)}
				       textInputInvalid="Неверный пароль"/>
				<PrimaryButton className={classes.button} type="submit" text="Войти" isDisabled={submitButtonDisabled}/>
			</form>
		</Paper>
	);
};

export default LoginForm;