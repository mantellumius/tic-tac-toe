import React from "react";
import classes from "./TableRow.module.css";

const TableRow = ({row, keysOrder, isHeader}) => {
	return (
		<tr className={classes.row}>
			{isHeader ?
				row.map((header, i) => <th key={i}>{header}</th>) :
				keysOrder.map((key, i) => <td key={i}>{row[key]}</td>)}
		</tr>
	);
};

export default TableRow;