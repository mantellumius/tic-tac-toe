import React from "react";
import classes from "./Table.module.css";
import TableRow from "./TableRow/TableRow";

const Table = ({headers, rows, keysOrder}) => {
	return (
		<table className={classes.table}>
			<thead>
				<TableRow row={headers} keysOrder={keysOrder} isHeader/>
			</thead>
			<tbody>
				{rows && rows.map((row, i) =>
					<TableRow key={i} row={row} keysOrder={keysOrder}/>)}
			</tbody>
		</table>
	);
};

export default Table;