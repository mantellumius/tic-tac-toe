import React from "react";
import classes from "./Label.module.css";

const Label = ({text}) => {
	return (
		<div className={classes.label}>
			{text}
		</div>
	);
};

export default Label;