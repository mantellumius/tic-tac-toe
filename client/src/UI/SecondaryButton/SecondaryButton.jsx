import React from "react";
import classes from "./SecondaryButton.module.css";

const SecondaryButton = ({type, isDisabled, text, className, onClick, width, height, ...props}) => {
	return (
		<button {...props}
		        style={{width: width, height: height}}
		        onClick={onClick}
		        disabled={isDisabled}
		        className={`${classes.button} ${className ? className : ""}`}
		        type={type}>
			{text}{props.children}
		</button>
	);
};

export default SecondaryButton;