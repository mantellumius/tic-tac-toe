import React from "react";
import Icon from "../Icon/Icon";
import classes from "./IconButton.module.css";

// eslint-disable-next-line no-unused-vars
const IconButton = ({icon, onClick}) => {
	return (
		<div onClick={onClick} className={classes.iconButton}>
			<Icon icon={icon}/>
		</div>
	);
};

export default IconButton;