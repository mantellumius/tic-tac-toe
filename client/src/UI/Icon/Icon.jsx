import React from "react";
import classes from "./Icon.module.css";

const Icon = ({icon, width, height}) => {
	return (
		<div className={classes.icon}>
			<img src={icon} alt="" style={{width: width, height: height}}/>
		</div>
	);
};

export default Icon;