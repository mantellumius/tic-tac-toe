import React from "react";
import classes from "./Paper.module.css";

const Paper = ({children, width, height, className}) => {
	return (
		<div style={{width: width, height: height}} className={`${classes.paper} ${className}`}>
			{children}
		</div>
	);
};

export default Paper;