import React from "react";
import classes from "./ChipsS.module.css";

const ChipsS = ({type}) => {
	function renderSwitch() {
		switch (type) {
		case "ready":
			return <div className={`${classes.ready} ${classes.base}`}>Свободен</div>;
		case "blocked":
			return <div className={`${classes.blocked} ${classes.base}`}>Заблокирован</div>;
		case "playing":
			return <div className={`${classes.playing} ${classes.base}`}>В игре</div>;
		default:
			throw new Error("Unknown chips type");
		}
	}

	return (
		<>
			{renderSwitch(type)}
		</>
	);
};

export default ChipsS;