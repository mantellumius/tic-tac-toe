import React from "react";
import classes from "./ChipsM.module.css";

const ChipsM = ({type}) => {
	function renderSwitch() {
		switch (type) {
		case "ready":
			return <div className={`${classes.ready} ${classes.base}`}>Активен</div>;
		case "blocked":
			return <div className={`${classes.blocked} ${classes.base}`}>Заблокирован</div>;
		case "playing":
			return <div className={`${classes.playing} ${classes.base}`}>Играет</div>;
		default:
			throw new Error("Unknown chips type");
		}
	}

	return (
		<>
			{renderSwitch(type)}
		</>
	);
};

export default ChipsM;