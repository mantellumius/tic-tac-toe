import React from "react";
import classes from "./Input.module.css";

export default function Input({
	                              id, onCopy, name, type, placeholder, width, height,
	                              value, onChange, isInputInvalid, textInputInvalid, ...props
}) {
	return (<div>
		<input {...props}
			       value={value}
			       style={{width: width, height: height}}
			       id={id}
			       onChange={onChange}
			       className={`${classes.input} ${isInputInvalid ? classes.invalidInput : ""}`}
			       type={type}
			       placeholder={placeholder}
			       onCopy={onCopy}
			       name={name}/>
		{isInputInvalid && <div className={classes.invalidInputText}>{textInputInvalid}</div>}
	</div>
	);
}