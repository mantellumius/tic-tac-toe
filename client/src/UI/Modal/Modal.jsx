import React from "react";
import classes from "./Modal.module.css";

const Modal = ({children, setIsOpen, width, height, className}) => {
	return (
		<div className={classes.background} onClick={() => setIsOpen(false)}>
			<div className={`${classes.modal} ${className}`} onClick={(e) => e.stopPropagation()}
			     style={{width: width, height: height}}>
				{children}
			</div>
		</div>
	);
};

export default Modal;