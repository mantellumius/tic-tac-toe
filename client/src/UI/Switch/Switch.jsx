import React, {useState} from "react";
import classes from "./Switch.module.css";

const Switch = ({text, callback}) => {
	const [on, setOn] = useState(false);

	function onClick() {
		setOn(!on);
		if (typeof callback === "function")
			callback(!on);
	}

	return (
		<div className={classes.switch}>
			<div>{text}</div>
			<div className={classes.interactive} onClick={onClick}>
				{on ? <img src="images/switchon.svg" alt={"switchOn"}/> : <img src="images/switchoff.svg" alt={"switchOff"}/>}
			</div>
		</div>
	);
};

export default Switch;