import React from "react";
import classes from "./Header.module.css";

const Header = ({children, className, ...props}) => {
	return (
		<h1 {...props} className={`${classes.header} ${className}`}>{children}</h1>
	);
};

export default Header;