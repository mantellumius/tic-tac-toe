import React from "react";
import classes from "./RadioButton.module.css";

const RadioButton = ({children, onClick, on}) => {
	return (
		<div className={`${classes.radioButton} ${on ? classes.on : classes.off}`} onClick={onClick}>
			{children}
		</div>
	);
};

export default RadioButton;