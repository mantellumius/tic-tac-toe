import React from "react";
import RadioButton from "./RadioButton/RadioButton";
import classes from "./RadioButtonSet.module.css";

const RadioButtonSet = ({settings, setSettings, onChange}) => {
	function onClick(index) {
		setSettings(settings.map((setting, i) =>
			new Object({...setting, boolean: index === i})));
		if (typeof onChange === "function")
			onChange(settings[index].value);
	}

	return (
		<div className={classes.radioButtonSet}>
			{settings.map((setting, index) =>
				<RadioButton key={setting.value} on={setting.boolean} onClick={() => onClick(index)}>
					{setting.content}
				</RadioButton>)}
		</div>
	);
};

export default RadioButtonSet;