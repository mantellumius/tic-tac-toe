import React from "react";
import classes from "./InputSet.module.css";

const InputSet = ({children, width}) => {
	return (
		<div className={classes.inputSet} style={{width: width}}>
			{children}
		</div>
	);
};

export default InputSet;