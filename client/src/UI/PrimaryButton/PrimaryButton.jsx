import React from "react";
import classes from "./PrimaryButton.module.css";

const PrimaryButton = ({type, isDisabled, text, className, onClick, width, height, ...props}) => {
	return (
		<button {...props}
		        onClick={onClick}
		        disabled={isDisabled}
		        className={`${classes.button} ${className}`}
		        type={type}
		        style={{width: width, height: height}}>
			{text}{props.children}
		</button>
	);
};

export default PrimaryButton;