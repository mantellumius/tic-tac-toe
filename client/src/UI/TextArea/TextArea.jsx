import React from "react";
import classes from "./TextArea.module.css";

const TextArea = ({placeholder, value, onChange,onKeyDown}) => {
	return (
		<textarea value={value} onChange={onChange} onKeyDown={onKeyDown} className={classes.textarea} placeholder={placeholder}></textarea>
	);
};

export default TextArea;