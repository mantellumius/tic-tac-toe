import React from "react";
import classes from "./RectangleTemplate.module.css";

const RectangleTemplate = ({width, height}) => {
	return (
		<div className={classes.rectangle} style={{width, height}}>

		</div>
	);
};

export default RectangleTemplate;