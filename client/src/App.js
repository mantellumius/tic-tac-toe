import React from "react";
import Navbar from "./components/Navbar/Navbar";
import {StoreProvider} from "./stores/AuthStore";
import SnackbarProvider from "./components/SnackbarProvider/SnackbarProvider";
import Router from "./components/Router/Router";
import {useNavigate} from "react-router-dom";

function App({stores}) {
	stores.playgroundStore.navigate = useNavigate();
	return (
		<StoreProvider store={stores.authStore}>
			<Navbar/>
			<SnackbarProvider snackbarStore={stores.snackbarStore}/>
			<Router stores={stores}/>
		</StoreProvider>
	);
}

export default App;
