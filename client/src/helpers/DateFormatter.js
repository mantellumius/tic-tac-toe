const monthToString = {
	0: "января",
	1: "февраля",
	2: "марта",
	3: "апреля",
	4: "мая",
	5: "июня",
	6: "июля",
	7: "августа",
	8: "сентября",
	9: "октября",
	10: "ноября",
	11: "декабря"
};

export const FormatDate = (inputDate) => {
	let date = new Date(inputDate);
	return date.getDate() + " " + monthToString[date.getMonth()] + " " + date.getFullYear();
};

export const GetTime = (inputDate) => {
	let date = new Date(inputDate);
	return `${date.getHours()}:${date.getMinutes() < 10 ? "0" : ""}${date.getMinutes()}`;
};

export const FormatDiff = (span) => {

	let diff = span / 1000;
	const minutes = Math.floor(diff / 60);
	const seconds = Math.round(diff % 60);
	return `${minutes} мин ${seconds} сек`;
};