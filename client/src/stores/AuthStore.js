import {makeAutoObservable, runInAction} from "mobx";
import {createContext, useContext} from "react";
import $api, {API_URL} from "../api";

export default class AuthStore {
	isAuth = false;
	user = null;
	onLogout = [];
	onCheckAuth = [];

	get userId() {
		return this.user?.id;
	}

	roles = {
		user: 1,
		admin: 2,
	};

	constructor() {
		makeAutoObservable(this);
	}

	static async create() {
		const store = new AuthStore();
		await store.checkAuth();
		return store;
	}

	async login(user) {
		const response = await $api.post("/auth/login", {...user});
		if (response?.status === 200) {
			this.isAuth = true;
			this.user = response.data?.user;
			localStorage.setItem("accessToken", response.data?.accessToken);
			await this.checkAuth();
		}
		return this.isAuth;
	}

	async logout() {
		await $api.get("/auth/logout");
		localStorage.removeItem("accessToken");
		this.isAuth = false;
		this.onLogout.forEach(func => {
			if (typeof func === "function") func();
		});
	}

	async checkAuth() {
		try {
			const response = await $api.get(`${API_URL}/auth/fetch-user-info`);
			if (response?.status === 200) {
				runInAction(() => {
					this.isAuth = true;
					this.user = response.data;
				});
			}
			this.onCheckAuth.forEach(func => {
				if (typeof func === "function") func();
			});
		} catch (e) {
			console.error(e);
		}
	}

	haveAccess(role) {
		if (this.user?.statusIsBlocked) return false;
		role = this.roles.hasOwnProperty(role) ? role : Object.keys(this.roles)[0];
		return this.isAuth && this.roles[this.user?.role] >= this.roles[role];
	}
}

const StoreContext = createContext(null);
export const StoreProvider = ({children, store}) => {
	return (
		<StoreContext.Provider value={store}>
			{children}
		</StoreContext.Provider>
	);
};
export const useAuthStore = () => useContext(StoreContext);
