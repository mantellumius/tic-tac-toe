import {makeAutoObservable} from "mobx";
import React, {createContext, useContext} from "react";
import {events} from "shared";
import {GetTime} from "../helpers/DateFormatter";
import $api from "../api";

export default class PlaygroundStore {
	authStore;
	socketStore;
	gameId;
	playersInfo = null;
	messages = [];
	state = [[null, null, null], [null, null, null], [null, null, null]];
	isGameOver = false;
	mode = "x";
	matchResult = {};
	navigate;
	gameBegin;

	constructor(authStore, socketStore, snackbarStore) {
		this.socketStore = socketStore;
		this.authStore = authStore;
		this.snackbarStore = snackbarStore;
		this.messages = [];
		this.state = [[null, null, null], [null, null, null], [null, null, null]];
		this.init();
		makeAutoObservable(this);
	}

	async init() {
		this.addEvents();
	}

	addEvents() {
		this.socketStore.addEvent(events.game["invite-accepted"], async ({gameId}) => {
			await this.onInviteAccepted(gameId);
		});
		this.socketStore.addEvent(events.game["invite-sent"], ({opponentId, fullName}) => {
			this.onInviteSent(opponentId, fullName);
		});
		this.socketStore.addEvent(events.game["send-state"],
			async ({gameId, state, isGameOver, mode, matchResult, messages, gameBegin}) => {
				await this.onSendState(gameId, state, isGameOver, mode, matchResult, messages, gameBegin);
			});
		this.socketStore.addEvent(events.game["message-sent"], ({userId, text}) => {
			this.onMessageSent(userId, text);
		});
		this.socketStore.addEvent(events.game["move-made"], ({userId, x, y}) => {
			this.onMoveMade(userId, x, y);
		});
		this.socketStore.addEvent(events.game["finished"], ({matchResult}) => {
			this.onGameOver(matchResult);
		});
		this.socketStore.addEvent(events.game["move-failed"], () => {
			console.error("illegal move");
		});
	}

	async onInviteAccepted(gameId) {
		this.gameId = gameId;
		this.state = [[null, null, null], [null, null, null], [null, null, null]];
		this.gameBegin = new Date(Date.now());
		this.mode = "x";
		this.messages = [];
		this.matchResult = null;
		this.isGameOver = false;
		this.navigate("/playground");
		await this.fetchPlayersInfo(this.gameId);
	}

	onInviteSent(opponentId, fullName) {
		const accept = () => {
			// if (!this.isGameOver) return console.error("Game is not over");
			this.socketStore.socket.emit(events.game["accept-invite"], {
				userId: this.authStore.userId,
				opponentId: opponentId,
			});
		};
		this.snackbarStore.addSnackbar({
			id: `${opponentId}-${this.authStore.userId}`,
			type: "invite",
			text: `Приглашение в игру от ${fullName.firstName} ${fullName.surName}`,
			action: {
				redirectTo: "/playground",
				text: "Принять",
				callback() {
					accept();
					this.remove();
				}
			}
		});
	}

	invite(opponentId) {
		this.socketStore.socket.emit(events.game["send-invite"], {
			userId: this.authStore.userId,
			opponentId: opponentId
		});
	}

	sendMessage(userId, text) {
		if (!this.playersInfo) return;
		this.socketStore.socket.emit(events.game["send-message"], {userId: userId, text: text});
	}

	onMessageSent(userId, text) {
		if (!this.playersInfo) return;
		let user = this.playersInfo.get(userId);
		let newMessage = {
			id: this.messages.length,
			author: user.nickname,
			mode: user.mode,
			time: GetTime(Date.now()),
			side: user.id === this.authStore.userId ? "me" : "other",
			text: text,
		};
		this.messages.push(newMessage);
	}


	makeMove(userId, x, y) {
		if (!this.playersInfo) return;
		this.socketStore.socket.emit(events.game["make-move"], {userId, x, y});
	}

	onMoveMade(userId, x, y) {
		this.state[y][x] = this.playersInfo.get(userId).mode;
		this.mode = this.mode === "x" ? "o" : "x";
	}

	onGameOver(matchResult) {
		this.isGameOver = true;
		this.matchResult = {winnersCombination: matchResult.winnersCombination};
		if (matchResult.xWin) this.matchResult["winnerNickname"] = this.playersInfo.get("x").nickname;
		else if (matchResult.oWin) this.matchResult["winnerNickname"] = this.playersInfo.get("o").nickname;
	}

	async onSendState(gameId, state, isGameOver, mode, matchResult, messages, gameBegin) {
		await this.fetchPlayersInfo(gameId);
		this.gameId = gameId;
		this.state = state;
		this.isGameOver = isGameOver;
		this.gameBegin = gameBegin;
		this.mode = mode;
		this.messages = messages ? messages.map((message, i) => {
			let user = this.playersInfo.get(message.idUser);
			return {
				id: i,
				author: user.nickname,
				mode: user.mode,
				time: GetTime(Date.now()),
				side: user.id === this.authStore.userId ? "me" : "other",
				text: message.text,
			};
		}) : [];
		this.matchResult = matchResult;
		if (isGameOver) this.onGameOver(matchResult);
	}

	async fetchPlayersInfo(gameId) {
		if (!gameId) return;
		let players = (await $api.get(`/protected/get-players-info/${gameId}`))?.data;
		players = players.map(player => {
			return {
				...player,
				name: `${player.surName} ${player.firstName} ${player.secondName}`,
				nickname: `${player.firstName} ${player.surName}`
			};
		});
		this.playersInfo = new Map([
			["x", players.find(player => player.mode === "x")],
			["o", players.find(player => player.mode === "o")],
			[players[0].id, players.find(player => player.id === players[0].id)],
			[players[1].id, players.find(player => player.id === players[1].id)],
		]);
	}
}

const StoreContext = createContext(null);
export const StoreProvider = ({children, store}) => {
	return (
		<StoreContext.Provider value={store}>
			{children}
		</StoreContext.Provider>
	);
};
export const usePlaygroundStore = () => useContext(StoreContext);
