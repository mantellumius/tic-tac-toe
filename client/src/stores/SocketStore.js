import React from "react";
import io from "socket.io-client";

export default class SocketStore {
	authStore;
	socket = null;
	eventsToCallback = {};

	constructor(authStore) {
		this.authStore = authStore;
		this.connect();
		this.authStore.onCheckAuth.push(this.connect.bind(this));
		this.authStore.onLogout.push(this.disconnect.bind(this));
	}

	connect() {
		if (this.authStore.isAuth) {
			this.socket = io("http://localhost:3001", {query: {userId: this.authStore.userId}});
			this.socket.on("connect", () => {
				for (let event in this.eventsToCallback)
					this.socket.on(event, this.eventsToCallback[event]);
			});
		}
	}

	disconnect() {
		this.socket.disconnect();
	}

	addEvent(event, callback) {
		this.eventsToCallback[event] = callback;
	}
}

const StoreContext = React.createContext(null);
export const StoreProvider = ({children, store}) => {
	return (
		<StoreContext.Provider value={store}>
			{children}
		</StoreContext.Provider>
	);
};
export const usePlaygroundStore = () => React.useContext(StoreContext);
