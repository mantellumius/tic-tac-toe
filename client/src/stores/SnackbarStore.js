import {createContext, useContext} from "react";
import {makeAutoObservable} from "mobx";

export default class SnackbarStore {
	snackbars = new Map();

	constructor() {
		makeAutoObservable(this);
	}

	addSnackbar(snackbar) {
		if (this.snackbars.has(snackbar.id)) return;
		if (snackbar.action)
			snackbar.action.callback = snackbar.action.callback.bind(snackbar);
		snackbar.remove = this.removeSnackbar.bind(this, snackbar.id);
		this.snackbars.set(snackbar.id, snackbar);
	}

	removeSnackbar(id) {
		this.snackbars.delete(id);
		this.snackbars.forEach((snackbar) => {
			if (snackbar.type === "invite")
				snackbar.remove();
		});
	}
}

const StoreContext = createContext(null);
export const StoreProvider = ({children, store}) => {
	return (
		<StoreContext.Provider value={store}>
			{children}
		</StoreContext.Provider>
	);
};
export const useAuthStore = () => useContext(StoreContext);
