import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import AuthStore from "./stores/AuthStore";
import SocketStore from "./stores/SocketStore";
import PlaygroundStore from "./stores/PlaygroundStore";
import SnackbarStore from "./stores/SnackbarStore";
import {BrowserRouter} from "react-router-dom";

(async function main() {
	const authStore = await AuthStore.create();
	const snackbarStore = new SnackbarStore();
	const socketStore = new SocketStore(authStore);
	const playgroundStore = new PlaygroundStore(authStore, socketStore, snackbarStore);
	const root = ReactDOM.createRoot(document.getElementById("root"));
	root.render(
		<React.StrictMode>
			<BrowserRouter>
				<App stores={{authStore, snackbarStore, socketStore, playgroundStore}}/>
			</BrowserRouter>
		</React.StrictMode>
	);
})();


