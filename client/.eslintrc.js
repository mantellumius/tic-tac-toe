module.exports = {
	"env": {
		"browser": true,
		"es2021": true
	},
	"extends": [
		"eslint:recommended",
		"plugin:react/recommended"
	],
	"overrides": [],
	"parserOptions": {
		"ecmaVersion": "latest",
		"sourceType": "module"
	},
	"plugins": [
		"react"
	],
	"rules": {

		"linebreak-style": [
			"error",
			"unix"
		],
		"quotes": [
			"error",
			"double"
		],
		"semi": [
			"error",
			"always"
		],
		// "no-unused-vars": ["warning"],
		"indent": "off",
		"react/prop-types": "off",
		"react/react-in-jsx-scope": "off",
		"no-prototype-builtins": "off",
		"no-mixed-spaces-and-tabs": "off"
	}
};
