/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
import {config} from "dotenv";

config();
export default {
	development: {
		client: "pg",
		connection: process.env.DB_DEV_URL,
		pool: {
			min: 2,
			max: 10
		},
		migrations: {
			tableName: "knex_migrations"
		}
	},
	staging: {
		client: "pg",
		connection: process.env.DB_DEV_URL,
		pool: {
			min: 2,
			max: 10
		},
		migrations: {
			tableName: "knex_migrations"
		}
	},
	production: {
		client: "pg",
		connection: process.env.DB_PROD_URL,
		pool: {
			min: 2,
			max: 10
		},
		migrations: {
			tableName: "knex_migrations"
		}
	}
};
