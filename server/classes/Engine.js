import {Game} from "./Game.js";

class Engine {
	static create(state) {
		const game = new Game();
		game.field = state || [[null, null, null], [null, null, null], [null, null, null]];
		game.mode = Engine.calculateMode(game.field);
		Engine.updateGameFieldStatus(game);
		return game;
	}

	static calculateMode(field) {
		let xCount = 0, oCount = 0;
		for (let x = 0; x < 3; x++)
			for (let y = 0; y < 3; y++)
				if (field[y][x] === "x")
					xCount++;
				else if (field[y][x] === "o")
					oCount++;
		return (xCount > oCount) ? "o" : "x";
	}

	static tryMakeMove(game, x, y) {
		if (!Engine.canMove(game, x, y)) return false;
		game.field[y][x] = game.mode;
		Engine.updateGameFieldStatus(game);
		game.mode = game.mode === "x" ? "o" : "x";
		return true;
	}

	static makeMove(field, mode, x, y) {
		field[y][x] = mode;
		return field;
	}

	static getPossibleMoves(field) {
		const result = [];
		for (let x = 0; x < 3; x++)
			for (let y = 0; y < 3; y++)
				if (Engine.isCellAvailable(field, x, y))
					result.push([x, y]);
		return result;
	}

	static canMove(game, x, y) {
		return !game.isGameOver && Engine.isCellAvailable(game.field, x, y);
	}

	static isCellAvailable(field, x, y) {
		return (y < field.length && y >= 0 && x < field[y].length && x >= 0 && field[y][x] == null);
	}

	static updateGameFieldStatus(game) {
		let matchResult = Engine.getMatchResult(game.field);
		if (matchResult.xWin || matchResult.oWin || Engine.isFieldFilled(game.field)) {
			game.isGameOver = true;
			game.matchResult = matchResult;
		}
	}

	static isFieldFilled(field) {
		return !field.flat().some(x => x == null);
	}

	static getMatchResult(field) {
		let flattenedGameField = field.flat();
		let result = {
			xWin: false,
			oWin: false,
			draw: false,
			winnersCombination: [],
		};
		if (Engine.isFieldFilled(field)) result.draw = true;
		for (let winCondition of winCombinations) {
			let oCount = 0;
			let xCount = 0;
			for (let i of winCondition) {
				if (flattenedGameField[i] === "x") xCount++;
				else if (flattenedGameField[i] === "o") oCount++;
			}
			if (xCount === 3) {
				result.xWin = true;
				result.winnersCombination = winCondition;
			}
			if (oCount === 3) {
				result.oWin = true;
				result.winnersCombination = winCondition;
			}
		}
		if (result.xWin || result.oWin) result.draw = false;
		return result;
	}

	static evaluateField(field) {
		const result = Engine.getMatchResult(field);
		if (result.xWin || result.oWin || result.draw) {
			if (result.xWin) return 1;
			if (result.oWin) return -1;
			if (result.draw) return 0;
		}
		const possibleMoves = Engine.getPossibleMoves(field);
		const mode = Engine.calculateMode(field);
		const evaluations = possibleMoves.map(([x, y]) => Engine.evaluateField(Engine.makeMove(JSON.parse(JSON.stringify(field)), mode, x, y)));
		return mode === "x" ? Math.max(...evaluations) : Math.min(...evaluations);
	}

	static getBestMoves(field) {
		const result = Engine.getMatchResult(field);
		if (result.xWin || result.oWin || result.draw) return [];
		const possibleMoves = Engine.getPossibleMoves(field);
		const mode = Engine.calculateMode(field);
		const evaluations = possibleMoves.map(step => [step, Engine.evaluateField(Engine.makeMove(JSON.parse(JSON.stringify(field)), mode, step[0], step[1]))]);
		if (mode === "x") {
			evaluations.sort((a, b) => b[1] - a[1]);
			const max = evaluations[0][1];
			return evaluations.filter(x => x[1] === max).map(x => x[0]);
		} else {
			evaluations.sort((a, b) => a[1] - b[1]);
			const min = evaluations[0][1];
			return evaluations.filter(x => x[1] === min).map(x => x[0]);
		}
	}
}

const winCombinations = [
	[0, 1, 2], [3, 4, 5], [6, 7, 8],
	[0, 3, 6], [1, 4, 7], [2, 5, 8],
	[0, 4, 8], [2, 4, 6]
];
export default Engine;