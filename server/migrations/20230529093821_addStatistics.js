/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = function (knex) {
	return knex.schema.createTable("counters", table => {
		table.integer("idUser").index().references("id").inTable("users").onDelete("CASCADE");
		table.timestamp("month", {useTz: false});
		table.specificType("side", "character");
		table.primary(["idUser", "month", "side"]);
		table.integer("wins");
		table.integer("loses");
		table.integer("draws");
	});
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = function (knex) {
	return knex.schema.dropTable("counters");
};
