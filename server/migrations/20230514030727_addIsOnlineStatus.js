/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = function (knex) {
	return knex.schema.table("users", table => {
		table.boolean("statusIsOnline").notNullable().defaultTo(false);
	});
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = function (knex) {
	return knex.schema.table("users", table => {
		table.dropColumn("statusIsOnline");
	});
};
