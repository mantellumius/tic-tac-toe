/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = function (knex) {
	return knex.schema.createTable("tokens", table => {
		table.integer("idUser").unique().index().references("id").inTable("users").onDelete("CASCADE");
		table.string("refreshToken").notNullable();
	});
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = function (knex) {
	return knex.schema.dropTable("tokens");
};
