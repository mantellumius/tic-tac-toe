/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const up = function (knex) {
	return knex.schema
		.createTable("users", function (table) {
			table.increments("id").primary().index().unique();
			table.string("surName", 255).notNullable();
			table.string("firstName", 255).notNullable();
			table.string("secondName", 255).notNullable();
			table.integer("age").notNullable();
			table.timestamp("createdAt", {useTz: false}).defaultTo(knex.fn.now()).notNullable();
			table.timestamp("updatedAt", {useTz: false}).defaultTo(knex.fn.now()).notNullable();
			table.boolean("statusIsInGame").notNullable().defaultTo(false);
			table.boolean("statusIsBlocked").notNullable().defaultTo(false);
			table.boolean("gender").notNullable();
		})
		.createTable("accounts", function (table) {
			table.integer("idUser").unique().index().references("id").inTable("users").onDelete("CASCADE");
			table.string("login", 255).unique().index().notNullable();
			table.string("password", 255).notNullable();
		})
		.createTable("games", function (table) {
			table.increments("id").primary().index();
			table.integer("idUserX").notNullable().references("id").inTable("users").onDelete("CASCADE");
			table.integer("idUserO").notNullable().references("id").inTable("users").onDelete("CASCADE");
			table.boolean("gameResult").nullable();
			table.timestamp("gameBegin", {useTz: false}).defaultTo(knex.fn.now()).notNullable();
			table.timestamp("gameEnd", {useTz: false});
		})
		.createTable("messenger", function (table) {
			table.integer("idUser").index().notNullable().references("id").inTable("users");
			table.integer("idGame").index().notNullable().references("id").inTable("games").onDelete("CASCADE");
			table.timestamp("createdAt", {useTz: false}).defaultTo(knex.fn.now()).notNullable();
			table.text("text").notNullable();
		});
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const down = function (knex) {
	return knex.schema
		.dropTable("accounts")
		.dropTable("messenger")
		.dropTable("games")
		.dropTable("users");
};
