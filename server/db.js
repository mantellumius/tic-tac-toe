import knex from "knex";
import {config} from "dotenv";

config();
const pg = knex({
	client: "pg",
	connection: process.env.DB_DEV_URL,
	searchPath: ["knex", "public"],
});

export default pg;
