import createRandomUser from "./faker/user.js";

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const seed = async function (knex) {
	await knex("users").del();
	const users = [];
	for (let i = 0; i < 2000; i++) {
		let newUser = createRandomUser();
		newUser.id = i;
		users.push(newUser);
	}
	await knex("users").insert(users);
};
