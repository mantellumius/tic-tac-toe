select month,
        side,
        sum(wins + loses + draws) as total,
        sum(wins) as wins,
        sum(loses) as loses,
        sum(draws) as draws
from counters
where counters."idUser" = 1
GROUP by Cube (month, side);

with cte as (Select month,
                    side,
                    sum(wins + loses + draws) as total,
                    sum(wins) as wins,
                    sum(loses) as loses,
                    sum(draws) as draws
            from counters
            where counters."idUser" = 1
            GROUP by month, side)
select month,
        side,
        sum(wins + loses + draws) over (order by month, side) as total,
        sum(wins) over (order by month, side) as wins,
        sum(loses) over (order by month, side) as loses,
        sum(draws) over (order by month, side) as draws
from cte;