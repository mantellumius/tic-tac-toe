import {generateGames} from "./faker/helpers.js";

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const seed = async function (knex) {
	await knex("games").del();
	const mayToNovember = {from: new Date("2022-05-01"), to: new Date("2022-11-30")};
	// only from May 2022 to November 2022 10%
	let games = generateGames({from: 0, to: 200}, mayToNovember, 200000);
	for (let i = 0; i < games.length; i += 10000)
		await knex("games").insert(games.slice(i, i + 10000));
	// afk from December 2022 to May 2023 10%
	games = generateGames({from: 201, to: 400}, mayToNovember, 150000);
	for (let i = 0; i < games.length; i += 10000)
		await knex("games").insert(games.slice(i, i + 10000));
	// increased activity month from December 2022 to May 2023
	const months = {
		december: {from: new Date("2022-12-01"), to: new Date("2022-12-31")},
		january: {from: new Date("2023-01-01"), to: new Date("2023-01-31")},
		february: {from: new Date("2023-02-01"), to: new Date("2023-02-28")},
		march: {from: new Date("2023-03-01"), to: new Date("2023-03-31")},
		april: {from: new Date("2023-04-01"), to: new Date("2023-04-30")},
		may: {from: new Date("2023-05-01"), to: new Date("2023-05-31")},
	};
	let idRange = {from: 401, to: 431};
	for (const month in months) {
		games = generateGames(idRange, months[month], 25000);
		for (let i = 0; i < games.length; i += 10000)
			await knex("games").insert(games.slice(i, i + 10000));
		idRange.from += 30;
		idRange.to += 30;
	}
	// rest
	games = generateGames({from: 401, to: 1999}, null, 1500000);
	await knex("games").insert(generateGames({from: 401, to: 2000}, null, 1));
	for (let i = 0; i < games.length; i += 10000)
		await knex("games").insert(games.slice(i, i + 10000));
};


