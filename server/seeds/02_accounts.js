/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const seed = async function (knex) {
	await knex("accounts").del();
	const password = "$2a$12$YpVrSDD9C1D8HVyjB/gEB.WkV16vhXz88lgR8YVyhjuXGrowl4U9i"; // password123
	let users = await knex.select("id").from("users").limit(10);
	const accounts = [];
	for (const user of users) {
		let newAccount = {idUser: user.id, login: user.id, password: password};
		accounts.push(newAccount);
	}
	await knex("accounts").insert(accounts);
};