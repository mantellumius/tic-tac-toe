import { fakerRU as faker } from "@faker-js/faker";

export default function createRandomUser() {
	const sex = faker.person.sexType();
	const secondName = faker.person.middleName(sex);
	const firstName = faker.person.firstName(sex);
	const surName = faker.person.lastName(sex);
	const gender = sex === "male";
	const createdAt = faker.date.past();
	return {
		createdAt,
		updatedAt: createdAt,
		age: faker.number.int({
			"min": 20,
			"max": 100,
		}),
		role:  "user",
		firstName,
		surName,
		secondName,
		gender,
	};
}
