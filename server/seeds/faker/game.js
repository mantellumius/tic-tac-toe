import {fakerRU as faker} from "@faker-js/faker";

export default function createRandomGame(idX, idO, options) {
	const randomIndex = faker.number.int({ min: 0, max: 2 });
	const values = [false, true, null];
	const gameResult = values[randomIndex];
	const gameBegin = faker.date.between({from: options?.from || "2022-05-01", to: options?.to || "2023-05-31"});
	const gameDuration = faker.number.int({min: 60 * 1000, max: 10 * 60 * 1000});
	return {
		idUserO: idO,
		idUserX: idX,
		gameResult,
		gameEnd: new Date(gameBegin.getTime() + gameDuration),
		gameBegin,
	};
}
