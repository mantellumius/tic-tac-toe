import {fakerRU as faker} from "@faker-js/faker";
import createRandomGame from "./game.js";

export const generateIdsInRange = (from, to) => {
	let idX = faker.number.int({min: from, max: to});
	let idO = faker.number.int({min: from, max: to});
	while (idX === idO)
		idO = faker.number.int({min: from, max: to});
	return {idX, idO};
};

export function generateGames(idRange, period, amount) {
	const games = [];
	for (let i = 0; i <= amount; i++) {
		let {idX, idO} = generateIdsInRange(idRange.from, idRange.to);
		games.push(createRandomGame(idX, idO, period));
	}
	return games;
}