/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export const seed = async function (knex) {
	await knex("counters").del();
	const query = `SELECT u.id                                              AS idUser,
                          date_trunc('month', g."gameBegin")                AS MONTH,
                          'X'                                               AS side,
                          count(g.id) FILTER (WHERE g."gameResult" = TRUE)  AS wins,
                          count(g.id) FILTER (WHERE g."gameResult" = FALSE) AS loses,
                          count(g.id) FILTER (WHERE g."gameResult" IS NULL) AS draws
                   FROM users AS u
                            JOIN games AS g
                                 ON u.id = g."idUserX" AND u."statusIsBlocked" = FALSE
                   GROUP BY u.id, date_trunc('month', g."gameBegin")
                   UNION
                   SELECT u.id                                              AS idUser,
                          date_trunc('month', g."gameBegin")                AS MONTH,
                          'O'                                               AS side,
                          count(g.id) FILTER (WHERE g."gameResult" = FALSE) AS wins,
                          count(g.id) FILTER (WHERE g."gameResult" = TRUE)  AS loses,
                          count(g.id) FILTER (WHERE g."gameResult" IS NULL) AS draws
                   FROM users AS u
                            JOIN games AS g
                                 ON u.id = g."idUserO" AND u."statusIsBlocked" = FALSE
                   GROUP BY u.id, date_trunc('month', g."gameBegin")`;
	let result = await knex.raw(query);
	result = result.rows.map(row => {
		return {
			idUser: row.iduser,
			month: row.month,
			side: row.side,
			loses: row.loses,
			wins: row.wins,
			draws: row.draws,
		};
	});
	for (let i = 0; i < result.length; i += 10000)
		await knex.table("counters").insert(result.slice(i, i + 10000));
};


