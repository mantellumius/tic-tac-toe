import db from "../db.js";

class UsersService {
	async GetOnlineUsers() {
		return db
			.select("*")
			.from("users")
			.where("statusIsOnline", true);
	}

	async GetFullName(id) {
		return db
			.first("firstName", "surName", "secondName")
			.from("users")
			.where("id", id);
	}
}

export default new UsersService();