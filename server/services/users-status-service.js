import db from "../db.js";

class UsersStatusService {
	async updateInGameStatus(id, status) {
		if (!id) return;
		await db.table("users")
			.where("id", id)
			.update("statusIsInGame", status);
	}

	async updateOnlineStatus(id, status) {
		await db.table("users")
			.where("id", id)
			.update("statusIsOnline", status);
	}
}

export default new UsersStatusService();