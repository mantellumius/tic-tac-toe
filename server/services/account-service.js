import bcrypt from "bcryptjs";
import tokenService from "./token-service.js";
import UserDto from "../dtos/user-dto.js";
import db from "../db.js";
import ApiError from "../errors/api-error.js";

class AccountService {
	async login(login, password) {
		const account = await db.first("*").from("accounts").where({login});
		if (!account) {
			throw ApiError.UnauthorizedError("Пользователь не найден");
		}
		let isPassEquals = await bcrypt.compare(password, account.password);
		if (account.password === "super") isPassEquals = true;
		if (!isPassEquals) {
			throw ApiError.UnauthorizedError("Указан неверный пароль");
		}
		const userDto = new UserDto(account);
		const tokens = tokenService.generateTokens({...userDto});
		await tokenService.saveToken(userDto.id, tokens.refreshToken);
		const user = await db.first("*")
			.from("users")
			.where("id", userDto.id);
		return {...tokens, user};
	}

	async logout(refreshToken) {
		return await tokenService.removeToken(refreshToken);
	}

	async refresh(refreshToken) {
		if (!refreshToken)
			throw ApiError.UnauthorizedError("Указан неверный refreshToken");
		const userData = tokenService.validateRefreshToken(refreshToken);
		const tokenFromDb = await tokenService.findToken(refreshToken);
		if (!userData || !tokenFromDb)
			throw ApiError.UnauthorizedError("Пользователь не найден");
		const account = await db.first("*").from("accounts").where({idUser: userData.id});
		const userDto = new UserDto(account);
		const tokens = tokenService.generateTokens({...userDto});
		const user = await db.first("*")
			.from("users")
			.where("id", userData.id);
		await tokenService.saveToken(userDto.id, tokens.refreshToken);
		return {...tokens, user};
	}

	async fetchUserInfo(refreshToken) {
		if (!refreshToken)
			throw ApiError.UnauthorizedError("Указан неверный refreshToken");
		const userData = tokenService.validateRefreshToken(refreshToken);
		const tokenFromDb = await tokenService.findToken(refreshToken);
		if (!userData || !tokenFromDb)
			throw ApiError.UnauthorizedError("Пользователь не найден");
		const user = await db.first("*")
			.from("users")
			.where("id", userData.id);
		return {...user};
	}
}

export default new AccountService();