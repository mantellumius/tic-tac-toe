import jwt from "jsonwebtoken";
import db from "../db.js";
import {config} from "dotenv";

config();

class TokenService {
	generateTokens(payload) {
		const accessToken = jwt.sign(payload, process.env.JWT_ACCESS_SECRET, {expiresIn: "30m"});
		const refreshToken = jwt.sign(payload, process.env.JWT_REFRESH_SECRET, {expiresIn: "30d"});
		return {
			accessToken,
			refreshToken
		};
	}

	validateAccessToken = (token) => this.#validateToken(token, process.env.JWT_ACCESS_SECRET);
	validateRefreshToken = (token) => this.#validateToken(token, process.env.JWT_REFRESH_SECRET);

	#validateToken(token, secret) {
		try {
			return jwt.verify(token, secret);
		} catch (e) {
			return null;
		}
	}

	async saveToken(userId, refreshToken) {
		let tokenData = await db.first("*").from("tokens").where("idUser", userId);
		if (tokenData) {
			tokenData.refreshToken = refreshToken;
			await db.first("*")
				.from("tokens")
				.where("idUser", userId)
				.update({refreshToken});
		} else {
			tokenData = await db.table("tokens").insert({idUser: userId, refreshToken});
		}
		return tokenData;
	}

	async removeToken(refreshToken) {
		return db.table("tokens").where({refreshToken}).del();
	}

	async findToken(refreshToken) {
		return db.table("tokens").where({refreshToken}).first();
	}
}

export default new TokenService();