import db from "../db.js";

class GamesService {
	async GetPlayers(gameId) {
		const players = await db
			.select("games.idUserX", "games.idUserO", "users.id", "users.firstName", "users.secondName", "users.surName")
			.from("games")
			.where("games.id", gameId)
			.join("users", function () {
				this
					.on("users.id", "games.idUserX")
					.orOn("users.id", "games.idUserO");
			});
		if (players.length === 0)
			return null;
		let [xPlayer, oPlayer] = players;
		if (xPlayer.id !== xPlayer.idUserX)
			[xPlayer, oPlayer] = [oPlayer, xPlayer];
		return {
			xPlayer,
			oPlayer
		};
	}

	async GetGamesWithPlayers(limit) {
		return db
			.select("games.*",
				"userX.firstName AS firstNameX", "userX.surName AS surNameX", "userX.secondName AS secondNameX",
				"userO.firstName AS firstNameO", "userO.surName AS surNameO", "userO.secondName AS secondNameO")
			.from("games")
			.whereNotNull("gameEnd")
			.join("users AS userX", "games.idUserX", "=", "userX.id")
			.join("users AS userO", "games.idUserO", "=", "userO.id")
			.orderBy("gameEnd", "desc")
			.limit(limit || 100);
	}

	async GetGames(limit) {
		return db
			.select("*")
			.from("games")
			.whereNotNull("gameEnd")
			.orderBy("gameEnd", "desc")
			.limit(limit);
	}

	async GetMessages(gameId) {
		return db.select("idUser", "createdAt", "text")
			.where({idGame: gameId})
			.from("messenger");
	}

	async GetGame(gameId) {
		return db.first("*")
			.from("games")
			.where({id: gameId});
	}

	async CreateGame(idUserX, idUserO) {
		return db
			.insert({idUserX, idUserO, gameBegin: new Date(Date.now())}, "id")
			.into("games");
	}
}

export default new GamesService();