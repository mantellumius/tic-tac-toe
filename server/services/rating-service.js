import db from "../db.js";
import moment from "moment";

class RatingService {
	async getRatings() {
		const query = `SELECT "idUser",
                              row_number() OVER (ORDER BY AVG(wins * (CASE WHEN side = 'X' THEN 0.9 ELSE 1 END) -
                                                              loses * (CASE WHEN side = 'X' THEN 1.1 ELSE 1 END) +
                                                              draws / 4) / count(DISTINCT MONTH) DESC) AS POSITION,
                              SUM(wins + loses + draws)                                                AS total,
                              SUM(wins)                                                                AS wins,
                              SUM(loses)                                                               AS loses,
                              SUM(draws)                                                               AS draws,
                              SUM(wins::FLOAT) / SUM(wins + loses + draws)                             AS win_rate,
                              u."surName",
                              u."secondName",
                              u."firstName"
                       FROM counters
                                JOIN users as u
                                     ON "idUser" = u.id
                       WHERE MONTH >= date_trunc('month', now()) - INTERVAL '6 month'
                         AND wins + loses + draws > 0
                       GROUP BY "idUser", u."surName", u."secondName", u."firstName"
                       HAVING SUM(wins + loses + draws) > 10
                       ORDER BY AVG(wins * (CASE WHEN side = 'X' THEN 0.9 ELSE 1 END) -
                                    loses * (CASE WHEN side = 'X' THEN 1.1 ELSE 1 END) +
                                    draws / 4) / count(DISTINCT MONTH) DESC;`;
		const result = await db.raw(query);
		return (result)?.rows;
	}

	async getRatingsRealTime() {
		const query = `With counters as (SELECT u.id                                              AS idUser,
                                                date_trunc('month', g."gameBegin")                AS MONTH,
                                                'X'                                               AS side,
                                                count(g.id) FILTER (WHERE g."gameResult" = TRUE)  AS wins,
                                                count(g.id) FILTER (WHERE g."gameResult" = FALSE) AS loses,
                                                count(g.id) FILTER (WHERE g."gameResult" IS NULL) AS draws
                                         FROM users AS u
                                                  JOIN games AS g
                                                       ON u.id = g."idUserX" AND u."statusIsBlocked" = FALSE
                                         GROUP BY u.id, date_trunc('month', g."gameBegin")
                                         UNION
                                         SELECT u.id                                              AS idUser,
                                                date_trunc('month', g."gameBegin")                AS MONTH,
                                                'O'                                               AS side,
                                                count(g.id) FILTER (WHERE g."gameResult" = FALSE) AS wins,
                                                count(g.id) FILTER (WHERE g."gameResult" = TRUE)  AS loses,
                                                count(g.id) FILTER (WHERE g."gameResult" IS NULL) AS draws
                                         FROM users AS u
                                                  JOIN games AS g
                                                       ON u.id = g."idUserO" AND u."statusIsBlocked" = FALSE
                                         GROUP BY u.id, date_trunc('month', g."gameBegin"))
                       SELECT idUser,
                              row_number() OVER (ORDER BY AVG(wins * (CASE WHEN side = 'X' THEN 0.9 ELSE 1 END) -
                                                              loses * (CASE WHEN side = 'X' THEN 1.1 ELSE 1 END) +
                                                              draws / 4) / count(DISTINCT MONTH) DESC) AS POSITION,
                              SUM(wins + loses + draws)                                                AS total,
                              SUM(wins)                                                                AS wins,
                              SUM(loses)                                                               AS loses,
                              SUM(draws)                                                               AS draws,
                              SUM(wins::FLOAT) / SUM(wins + loses + draws)                             AS win_rate,
                              u."surName",
                              u."secondName",
                              u."firstName"
                       FROM counters
                                JOIN users as u
                                     ON idUser = u.id
                       WHERE MONTH >= date_trunc('month', now()) - INTERVAL '6 month'
                         AND wins + loses + draws > 0
                       GROUP BY idUser, u."surName", u."secondName", u."firstName"
                       HAVING SUM(wins + loses + draws) > 10
                       ORDER BY AVG(wins * (CASE WHEN side = 'X' THEN 0.9 ELSE 1 END) -
                                    loses * (CASE WHEN side = 'X' THEN 1.1 ELSE 1 END) +
                                    draws / 4) / count(DISTINCT MONTH) DESC;`;
		const result = await db.raw(query);
		return (result)?.rows;
	}

	async UpdatePlayersStat(gameId) {
		const gameInfo = await db.first("*").from("games").where("id", gameId);
		const month = moment(new Date()).startOf("month").toDate();
		const isDraw = gameInfo.gameResult === null;
		const isXWin = gameInfo.gameResult === true;
		const isXLose = gameInfo.gameResult === false;
		const isOWin = gameInfo.gameResult === false;
		const isOLose = gameInfo.gameResult === true;
		await db.table("counters")
			.insert({
				idUser: gameInfo.idUserO,
				month: month,
				side: "O",
				loses: isOLose ? 1 : 0,
				wins: isOWin ? 1 : 0,
				draws: isDraw ? 1 : 0,
			})
			.onConflict(["idUser", "month", "side"])
			.merge({
				loses: isOLose ? db.raw("counters.loses + 1") : db.raw("counters.loses"),
				wins: isOWin ? db.raw("counters.wins + 1") : db.raw("counters.wins"),
				draws: isDraw ? db.raw("counters.draws + 1") : db.raw("counters.draws"),
			});
		await db.table("counters")
			.insert({
				idUser: gameInfo.idUserX,
				month: month,
				side: "X",
				loses: isXLose ? 1 : 0,
				wins: isXWin ? 1 : 0,
				draws: isDraw ? 1 : 0,
			})
			.onConflict(["idUser", "month", "side"])
			.merge({
				loses: isXLose ? db.raw("counters.loses + 1") : db.raw("counters.loses"),
				wins: isXWin ? db.raw("counters.wins + 1") : db.raw("counters.wins"),
				draws: isDraw ? db.raw("counters.draws + 1" ) : db.raw("counters.draws"),
			});
	}
}

export default new RatingService();