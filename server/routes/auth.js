import {Router} from "express";
import AccountController from "../controllers/account-controller.js";

const router = Router();

router.post("/login", AccountController.login);

router.get("/logout", AccountController.logout);

router.get("/refresh", AccountController.refresh);

router.get("/fetch-user-info", AccountController.fetchUserInfo);

export default router;
