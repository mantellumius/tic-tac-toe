import {Router} from "express";
import GamesService from "../services/games-service.js";

const router = Router();

router.get("/get-players-info/:gameId", async function (req, res) {
	const {gameId} = req.params;
	const playersInfo = (await GamesService.GetPlayers(gameId));
	if (!playersInfo)
		return res.sendStatus(404);
	const {xPlayer, oPlayer} = playersInfo;
	return res.json([
		{
			id: xPlayer.id,
			firstName: xPlayer.firstName,
			secondName: xPlayer.secondName,
			surName: xPlayer.surName,
			mode: "x",
			winRate: 23
		}, {
			id: oPlayer.id,
			firstName: oPlayer.firstName,
			secondName: oPlayer.secondName,
			surName: oPlayer.surName,
			mode: "o",
			winRate: 63
		}
	]);
});

export default router;
