import {Router} from "express";
import usersController from "../controllers/users-controller.js";

const router = Router();

router.get("/users", usersController.getAll);

router.get("/users/:id", usersController.getById);

router.post("/users", usersController.create);

router.patch("/users/:id", usersController.updateById);

router.delete("/users/:id", usersController.deleteById);

export default router; 
