import {Router} from "express";
import RatingService from "../services/rating-service.js";

const router = Router();

router.get("/ratings", async function (req, res) {
	let result = await RatingService.getRatings();
	res.json(result);
});

export default router;
