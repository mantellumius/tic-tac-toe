import {Router} from "express";
import GamesService from "../services/games-service.js";

const router = Router();

router.get("/games", async function (req, res) {
	let {limit} = req.query;
	let games = await GamesService.GetGames(limit);
	res.json(games);
});

export default router;
