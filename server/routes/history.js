import {Router} from "express";
import GamesService from "../services/games-service.js";

const router = Router();

router.get("/history", async function (req, res) {
	let {limit} = req.query;
	let games = await GamesService.GetGamesWithPlayers(limit);
	res.json(games);
});

export default router;
