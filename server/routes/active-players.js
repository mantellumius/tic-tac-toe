import {Router} from "express";
import UsersService from "../services/users-service.js";

const router = Router();

router.get("/active-players", async function (req, res) {
	let players = await UsersService.GetOnlineUsers();
	res.json(players);
});

export default router;
