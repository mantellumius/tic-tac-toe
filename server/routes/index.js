import auth from "./auth.js";
import ratings from "./ratings.js";
import getPlayersInfo from "./get-players-info.js";
import users from "./users.js";
import activePlayers from "./active-players.js";
import games from "./games.js";
import history from "./history.js";

export {
	auth,
	ratings,
	getPlayersInfo,
	users,
	activePlayers,
	games,
	history
};