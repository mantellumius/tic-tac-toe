const events = {
	game: {
		"send-message": "game:send-message",
		"message-sent": "game:message-sent",
		"make-move": "game:make-move",
		"move-made": "game:move-made",
		"move-failed": "game:move-failed",
		"send-state": "game:send-state",
		"finished": "game:over",
		"error": "game:error",
		"send-invite": "game:send-invite",
		"invite-sent": "game:invite-sent",
		"invite-accepted": "game:invite-accepted",
		"accept-invite": "game:accept-invite",
	}
};
export default events;