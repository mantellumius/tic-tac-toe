import GameHandler from "./event-handlers/game-handler.js";
import UsersStatusService from "./services/users-status-service.js";

export default class ioServer {
	connectedUsers = {};
	games = {};
	io;

	constructor(io) {
		this.io = io;
		this.registerHandlers();
	}

	registerHandlers() {
		this.io.on("connection", async (socket) => {
			socket.userId = socket.handshake.query.userId;
			if (!socket.userId)
				return socket.disconnect();
			await UsersStatusService.updateOnlineStatus(socket.userId, true);
			this.connectedUsers[socket.userId] = socket;
			const gameHandler = new GameHandler(this, socket);
			await gameHandler.onConnect(socket);
			await gameHandler.register(socket);
			socket.on("disconnect", async () => {
				await UsersStatusService.updateOnlineStatus(socket.userId, false);
				delete this.connectedUsers[socket.userId];
			});
		});
	}

	get to() {
		return this.io.to.bind(this.io);
	}
}