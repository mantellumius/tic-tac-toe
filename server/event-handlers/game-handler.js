import db from "../db.js";
import {events} from "shared";
import UsersStatusService from "../services/users-status-service.js";
import GamesService from "../services/games-service.js";
import UsersService from "../services/users-service.js";
import RatingService from "../services/rating-service.js";
import Engine from "../classes/Engine.js";

class GameHandler {
	socket;
	io;
	botId;

	constructor(io, socket, botId = 1) {
		this.io = io;
		this.socket = socket;
		this.botId = botId;
	}

	async onConnect(socket) {
		await UsersStatusService.updateInGameStatus(socket.userId, !!this.io.games[socket.userId]);
		if (this.io.games[socket.userId]) {
			await UsersStatusService.updateInGameStatus(socket.userId, true);
			socket.join(this.io.games[socket.userId].roomId);
			socket.emit(events.game["send-state"], await this.fetchGameState(socket.userId));
		}
	}

	async fetchGameState(userId) {
		let gameField = this.io.games[userId].field;
		let messages = await GamesService.GetMessages(this.io.games[userId].gameId);
		let game = await GamesService.GetGame(this.io.games[userId].gameId);
		return {
			gameId: this.io.games[userId].gameId,
			state: gameField.field,
			isGameOver: gameField.isGameOver,
			mode: gameField.mode,
			matchResult: gameField.matchResult,
			messages: messages,
			gameBegin: game.gameBegin,
		};
	}

	async register(socket) {
		socket.on(events.game["send-message"], this.sendMessage.bind(this));
		socket.on(events.game["make-move"], this.makeMove.bind(this));
		socket.on(events.game["send-invite"], this.sendInvite.bind(this));
		socket.on(events.game["accept-invite"], this.acceptInvite.bind(this));
		socket.on("disconnect", this.disconnect.bind(this));
	}

	async sendMessage(message) {
		await db.insert({idGame: this.io.games[message.userId]?.gameId, idUser: message.userId, text: message.text})
			.into("messenger");
		this.io.to(this.io.games[this.socket.userId].roomId).emit(events.game["message-sent"], message);
	}

	async makeMove({userId, x, y}) {
		let game = this.io.games[userId];
		if (game[game.field.mode] !== userId || !Engine.tryMakeMove(game.field, x, y)) {
			this.socket.emit(events.game["move-failed"], {});
			return false;
		}
		this.io
			.to(this.io.games[this.socket.userId].roomId)
			.emit(events.game["move-made"], {userId, x, y,});
		if (game.field.isGameOver)
			return await this.onGameFinished(this.socket, game);
		else if (this.io.games[userId][game.field.mode] === this.botId)
			return this.onMoveMade(game, userId);
	}

	async acceptInvite({userId, opponentId}) {
		const insertionResult = await GamesService.CreateGame(userId, opponentId);
		this.io.games[userId] = {
			field: Engine.create(),
			roomId: `${userId}-${opponentId}`,
			gameId: insertionResult[0].id,
			x: userId,
			o: opponentId,
		};
		this.io.games[opponentId] = this.io.games[userId];
		this.io.connectedUsers[opponentId].join(this.io.games[userId].roomId);
		this.io.connectedUsers[userId].join(this.io.games[userId].roomId);
		this.io.to(this.io.games[userId].roomId)
			.emit(events.game["invite-accepted"], {gameId: this.io.games[userId].gameId});
		await UsersStatusService.updateInGameStatus(this.io.connectedUsers[userId]?.userId, true);
		await UsersStatusService.updateInGameStatus(this.io.connectedUsers[opponentId]?.userId, true);
	}

	async sendInvite({userId, opponentId}) {
		let fullName = await UsersService.GetFullName(userId);
		if (opponentId !== this.botId)
			return this.io.connectedUsers[opponentId]?.emit(events.game["invite-sent"], {opponentId: userId, fullName});
		const insertionResult = await GamesService.CreateGame(userId, opponentId);
		this.io.games[userId] = {
			field: Engine.create(),
			roomId: `${userId}-${opponentId}`,
			gameId: insertionResult[0].id,
			x: userId,
			o: opponentId,
		};
		this.io.connectedUsers[userId].join(this.io.games[userId].roomId);
		this.io.to(this.io.games[userId].roomId)
			.emit(events.game["invite-accepted"], {gameId: this.io.games[userId].gameId});
		await UsersStatusService.updateInGameStatus(this.io.connectedUsers[userId]?.userId, true);
	}

	async disconnect(socket) {
		if (socket.userId)
			await UsersStatusService.updateInGameStatus(socket.userId, false);
	}

	async onGameFinished(socket, game) {
		this.io.to(this.io.games[socket.userId].roomId).emit(events.game["finished"], {matchResult: game.field.matchResult});
		let dbMatchResult = game.field.matchResult.xWin || game.field.matchResult.oWin ?
			game.field.matchResult.xWin :
			null;
		await db("games")
			.where("id", game.gameId)
			.update({
				gameEnd: new Date(Date.now()),
				gameResult: dbMatchResult
			});
		let {x, o} = game;
		await UsersStatusService.updateInGameStatus(this.io.connectedUsers[x]?.userId, false);
		await UsersStatusService.updateInGameStatus(this.io.connectedUsers[o]?.userId, false);
		await RatingService.UpdatePlayersStat(game.gameId);
		delete this.io.games[x];
		delete this.io.games[o];
	}

	async onMoveMade(game, userId) {
		const bestMoves = Engine.getBestMoves(game.field.field);
		const [x, y] = bestMoves[Math.floor(Math.random() * bestMoves.length)];
		if (!Engine.tryMakeMove(game.field, x, y)) return;
		setTimeout(async () => {
			this.io
				.to(this.io.games[userId].roomId)
				.emit(events.game["move-made"], {userId: this.botId, x, y,});
			if (game.field.isGameOver)
				return await this.onGameFinished(this.socket, game);
		}, 300);
	}
}


export default GameHandler;


