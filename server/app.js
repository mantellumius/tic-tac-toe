import express from "express";
import path from "path";
import cookieParser from "cookie-parser";
import {activePlayers, auth, games, getPlayersInfo, history, ratings, users} from "./routes/index.js";
import logger from "morgan";
import {fileURLToPath} from "url";
import {Server} from "socket.io";
import http from "http";
import ioServer from "./ioServer.js";
import errorMiddleware from "./middlewares/error-middleware.js";
import authMiddleware from "./middlewares/auth-middleware.js";
import cors from "cors";

process.env.TZ = "Etc/Universal";
const __dirname = path.dirname(fileURLToPath(import.meta.url));
const app = express();

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(cors({
	credentials: true,
	origin: "http://localhost:3000"
}));
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/api/protected", authMiddleware);
app.use("/api/auth", auth);
app.use("/api/protected", games);
app.use("/api/protected", history);
app.use("/api/protected", activePlayers);
app.use("/api/protected", users);
app.use("/api/protected", ratings);
app.use("/api/protected", getPlayersInfo);
app.use(errorMiddleware);

const server = http.createServer(app);
const io = new Server(server, {
	cors: {
		credentials: true,
		origin: "http://localhost:3000",
		methods: ["GET", "POST"]
	}
});

server.listen(process.env.PORT || 3001, () => {
	console.log("listening on http://localhost:3001");
});
new ioServer(io);

export default app;
