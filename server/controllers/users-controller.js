import db from "../db.js";

class UsersController {
	async getAll(req, res) {
		let players = await db
			.select("*")
			.from("users");
		players = players.map(player => {
			return {...player, id: player.id.toString(),};
		});
		res.json(players);
	}

	async getById(req, res) {
		let {id} = req.params;
		let player = await db
			.first("*")
			.from("users")
			.where("id", id);
		if (!player) return res.sendStatus(404);
		res.json({...player, id: player.id.toString()});
	}

	async create(req, res) {
		const result = await db("users").insert(req.body);
		res.json({id: result[0]});
	}

	async updateById(req, res) {
		const id = req.params.id;
		const result = await db("users")
			.where("id", id)
			.update(req.body);
		if (!result) return res.sendStatus(404);
		res.sendStatus(204);
	}

	async deleteById(req, res) {
		const id = req.params.id;
		const result = await db("users")
			.where("id", id)
			.del();
		if (!result) return res.sendStatus(404);
		res.sendStatus(204);
	}
}


export default new UsersController();