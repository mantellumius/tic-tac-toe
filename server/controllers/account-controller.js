import accountService from "../services/account-service.js";

class AccountController {
	async login(req, res, next) {
		try {
			const {login, password} = req.body;
			const userData = await accountService.login(login, password);
			res.cookie("refreshToken", userData.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true});
			return res.json(userData);
		} catch (e) {
			next(e);
		}
	}

	async logout(req, res, next) {
		try {
			const {refreshToken} = req.cookies;
			const token = await accountService.logout(refreshToken);
			res.clearCookie("refreshToken");
			return res.json(token);
		} catch (e) {
			next(e);
		}
	}

	async refresh(req, res, next) {
		try {
			const {refreshToken} = req.cookies;
			const userData = await accountService.refresh(refreshToken);
			res.cookie("refreshToken", userData.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true});
			return res.json(userData);
		} catch (e) {
			next(e);
		}
	}

	async fetchUserInfo(req, res, next) {
		try {
			const {refreshToken} = req.cookies;
			const userData = await accountService.fetchUserInfo(refreshToken);
			return res.json(userData);
		} catch (e) {
			next(e);
		}
	}
}


export default new AccountController();